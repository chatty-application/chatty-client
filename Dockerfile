FROM openjdk:11-oracle
EXPOSE 8384
COPY build/libs/*.jar chatty-client.jar
ENTRYPOINT ["java","-Dspring.profiles.active=dev","-jar","chatty-client.jar"]