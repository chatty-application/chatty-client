package chatty.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class ChattyClientApplication {
	public static void main(String[] args) {
		SpringApplication.run(ChattyClientApplication.class, args);
	}
}
