package chatty.client.controller;

import static chatty.client.controller.UserController.getActualUsername;
import static chatty.client.controller.UserController.getDefaultSession;
import static chatty.client.transformer.MessageTransformer.transform;
import static org.springframework.http.HttpEntity.EMPTY;
import static org.springframework.http.HttpMethod.GET;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import chatty.client.entity.Client;
import chatty.client.entity.Message;
import chatty.client.entity.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
public class MessageController {
	public StompSession session;
	private Client client = new Client();
	private List<User> users = new ArrayList<User>();
	private final RestTemplate restTemplate;

	private final String ALL = "all";
	private final String FORMATING = "/app/chat/%s";

	private final String USER_URL = "http://chatty-server-service:8383/user";

	@PostMapping
	public void sendMessage(@RequestBody Message message) throws InterruptedException, ExecutionException {
		session = getDefaultSession();
		client = Client.builder().name(getActualUsername()).topic(message.getTo()).build();
		session.send(String.format(FORMATING, ALL), transform(message, client));
	}

	@Scheduled(fixedDelayString = "${sheduler.fixed-delay}")
	public void checkNewUser() {
		List<User> usersOnServer = restTemplate.exchange(UriComponentsBuilder.fromUriString(USER_URL).build().encode().toUri(), GET, EMPTY,
						new ParameterizedTypeReference<List<User>>() {}).getBody();

		if (!usersOnServer.equals(users) && usersOnServer != null) {
			users = usersOnServer;
			log.info("Actual users is: {}", users);
		}
	}

}