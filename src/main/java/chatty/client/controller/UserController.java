package chatty.client.controller;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.POST;

import java.util.concurrent.ExecutionException;

import javax.annotation.PreDestroy;

import org.springframework.http.HttpEntity;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import chatty.client.config.WebSocketClientConfig;
import chatty.client.entity.Client;
import chatty.client.entity.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
public class UserController {

	private final RestTemplate restTemplate;
	private static StompSession session = null;
	private static User sessionUser = new User();

	private final String USER_URL = "http://chatty-server-service:8383/user";

	private void deleteUser(User sessionUser) {
		HttpEntity<User> httpEntity = new HttpEntity<User>(sessionUser);
		restTemplate.exchange(UriComponentsBuilder.fromUriString(USER_URL).build().encode().toUri(), DELETE, httpEntity,
				Object.class);
	}

	public StompSession makeDefaulSession(User sessionUser) {
		WebSocketClientConfig connector = new WebSocketClientConfig();
		try {
			return connector.configureWebsocket(
					Client.builder().name(sessionUser.getName()).topic(sessionUser.getName()).build());
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			return null;
		}
	}

	@PostMapping("/login")
	public void logIn(@RequestBody User user) {
		if (sessionUser.getName() == null) {
			HttpEntity<User> httpEntity = new HttpEntity<User>(user);
			restTemplate.exchange(UriComponentsBuilder.fromUriString(USER_URL).build().encode().toUri(), POST, httpEntity,
					Object.class);
			sessionUser = user;
			session = makeDefaulSession(sessionUser);
			log.info(sessionUser.toString());
		} else {
			deleteUser(sessionUser);
			HttpEntity<User> httpEntity = new HttpEntity<User>(user);
			restTemplate.exchange(UriComponentsBuilder.fromUriString(USER_URL).build().encode().toUri(), POST, httpEntity,
					Object.class);
			sessionUser = user;
			session = makeDefaulSession(sessionUser);
		}
	}

	@PreDestroy
	public void tearDown() {
		deleteUser(sessionUser);
	}

	public static String getActualUsername() {
		return sessionUser.getName();
	}

	public static StompSession getDefaultSession() {
		return session;
	}

}
