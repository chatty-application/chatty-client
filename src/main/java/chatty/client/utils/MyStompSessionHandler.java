package chatty.client.utils;

import static chatty.client.controller.UserController.getActualUsername;

import java.lang.reflect.Type;

import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.stereotype.Component;

import chatty.client.entity.Message;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@AllArgsConstructor
@NoArgsConstructor
public class MyStompSessionHandler extends StompSessionHandlerAdapter {
	@SuppressWarnings("unused")
	private Message message;

	private final String FORMATTING = "%s: %s";
	private final String ENDPOINT = "/topic/messages";

	private void subscribeTopic(StompSession session, String endpoint) {
		session.subscribe(endpoint, new StompFrameHandler() {
			@Override
			public Type getPayloadType(StompHeaders headers) {
				return Message.class;
			}

			@Override
			public void handleFrame(StompHeaders headers, Object payload) {
				Message receivedMessage = (Message) payload;
				if (getActualUsername() != null && getActualUsername().equals(receivedMessage.getTo())) {
					log.info(String.format(FORMATTING, receivedMessage.getFrom(), receivedMessage.getText()));
				}
			}
		});
	}

	@Override
	public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
		subscribeTopic(session, ENDPOINT);
	};
}
