package chatty.client.config;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import chatty.client.entity.Client;
import chatty.client.entity.Message;
import chatty.client.utils.MyStompSessionHandler;

@Component
public class WebSocketClientConfig {
	private final String TOPIC_URL = "ws://chatty-server-service:8383/messages";
	private final String TEXT = "default";

	private WebSocketClient simpleWebSocketClient = new StandardWebSocketClient();
	private List<Transport> transports = Arrays.asList(new WebSocketTransport(simpleWebSocketClient));

	private SockJsClient sockJsClient = new SockJsClient(transports);
	private WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);

	public StompSession configureWebsocket(Client client) throws InterruptedException, ExecutionException {

		stompClient.setMessageConverter(new MappingJackson2MessageConverter());
		StompSessionHandler sessionHandler = new MyStompSessionHandler(
				Message.builder().from(client.getName()).text(TEXT).to(client.getTopic()).build());
		StompSession session = stompClient.connect(TOPIC_URL, sessionHandler).get();
		return session;
	}

}
