package chatty.client.transformer;

import chatty.client.entity.Client;
import chatty.client.entity.Message;

public class MessageTransformer {
	public static Message transform(Message message, Client client) {
		return Message.builder()
				.from(client.getName())
				.text(message.getText())
				.to(message.getTo())
				.build();
	}

}
